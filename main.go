package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const (
	relSubscribe = "http://ostatus.org/schema/1.0/subscribe"
	activityType = "application/activity+json"
)

var httpClient = new(http.Client)

// User profile JSON representation, limited to the few fields we're
// interested in.
type userProfile struct {
	ID   string `json:"id"`
	Icon struct {
		Type string `json:"type"`
		URL  string `json:"url"`
	}
}

// Return the user icon, if any.
func (p *userProfile) icon() (string, bool) {
	s := p.Icon.URL
	return s, s != ""
}

// WebFinger JSON response, limited to the fields we're interested in.
type webfingerResponse struct {
	Links []struct {
		Rel      string `json:"rel"`
		Type     string `json:"type"`
		Href     string `json:"href"`
		Template string `json:"template"`
	} `json:"links"`
}

// Return the user profile link, if any.
func (w *webfingerResponse) userProfileLink() (string, bool) {
	for _, link := range w.Links {
		if link.Type == activityType {
			return link.Href, true
		}
	}
	return "", false
}

// Return the 'follow link' template, if any.
func (w *webfingerResponse) followLinkTemplate() (string, bool) {
	for _, link := range w.Links {
		if link.Rel == relSubscribe {
			return link.Template, true
		}
	}
	return "", false
}

// Simple HTTP GET request (with custom headers), expecting a JSON
// response which will be deserialized into 'obj'.
func jsonGET(ctx context.Context, uri string, hdrs map[string]string, obj interface{}) error {
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		return err
	}
	for k, v := range hdrs {
		req.Header.Set(k, v)
	}

	resp, err := httpClient.Do(req.WithContext(ctx))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return fmt.Errorf("HTTP status %d", resp.StatusCode)
	}

	return json.NewDecoder(resp.Body).Decode(obj)
}

// Perform a WebFinger lookup for the given identifier.
func webfingerLookup(ctx context.Context, id *identifier) (*webfingerResponse, error) {
	var wf webfingerResponse
	if err := jsonGET(ctx, id.webfingerURL(), nil, &wf); err != nil {
		return nil, err
	}
	return &wf, nil
}

// Fetch and parse a user profile located at the given URL.
func profileLookup(ctx context.Context, uri string) (*userProfile, error) {
	var up userProfile
	if err := jsonGET(ctx, uri, map[string]string{
		"Accept": "application/activity+json",
	}, &up); err != nil {
		return nil, err
	}
	return &up, nil
}

func handleFollowRequest(w http.ResponseWriter, req *http.Request) {
	if req.Method == "POST" {
		// Form submission.
		//
		// Here we have the "local" profile URL (we don't need
		// to trust it, as it's just sent as an argument to
		// the remote server), and the remote account, so we
		// attempt to find the "follow link template" for the
		// remote account via WebFinger, and redirect the user
		// there.
		remid, err := parseIdentifier(req.FormValue("remote-id"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		wf, err := webfingerLookup(req.Context(), remid)
		if err != nil {
			log.Fatalf("remote webfinger lookup error: %v", err)
			http.Error(w, fmt.Sprintf("Remote WebFinger lookup error: %v", err), http.StatusInternalServerError)
			return
		}

		tpl, ok := wf.followLinkTemplate()
		if !ok {
			http.Error(w, "No follow link in profile", http.StatusInternalServerError)
			return
		}

		profileURI := req.FormValue("local-profile-url")
		uri := strings.Replace(tpl, "{uri}", url.QueryEscape(profileURI), -1)
		http.Redirect(w, req, uri, http.StatusFound)
		return
	}

	// Show the form. We fetch some information from the "local"
	// user profile (via WebFinger) to show a nicer UI to the
	// user, with icons and such.
	locid, err := parseIdentifier(req.FormValue("local-id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	wf, err := webfingerLookup(req.Context(), locid)
	if err != nil {
		log.Printf("remote webfinger lookup error: %v", err)
		http.Error(w, fmt.Sprintf("Remote WebFinger lookup error: %v", err), http.StatusInternalServerError)
		return
	}
	profileURI, ok := wf.userProfileLink()
	if !ok {
		log.Printf("no user profile link: %s", locid)
		http.Error(w, "No user profile link in WebFinger response", http.StatusInternalServerError)
		return
	}

	// Icon is optional.
	var iconURL string
	p, err := profileLookup(req.Context(), profileURI)
	if err != nil {
		log.Printf("profile lookup error (%s): %v", profileURI, err)
	} else {
		profileURI = p.ID
		if s, ok := p.icon(); ok {
			iconURL = s
		}
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Cache-Control", "no-cache, no-store")
	formTpl.Execute(w, map[string]interface{}{
		"LocalID":          locid,
		"LocalProfileURL":  profileURI,
		"ProfileIconURL":   iconURL,
		"PlaceholderImage": placeholderImg,
	})
}

func handleFollowButton(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")
	w.Header().Set("Content-Length", strconv.Itoa(len(followButtonData)))
	w.Header().Set("ETag", followButtonETag)
	http.ServeContent(w, req, "follow.svg", time.Time{}, strings.NewReader(followButtonData))
}

var (
	addr = flag.String("addr", ":3976", "address to listen on")
)

func main() {
	flag.Parse()

	h := http.NewServeMux()
	h.HandleFunc("/follow.svg", handleFollowButton)
	h.HandleFunc("/follow", handleFollowRequest)

	err := http.ListenAndServe(*addr, http.TimeoutHandler(h, 10*time.Second, ""))
	if err != nil {
		log.Fatal(err)
	}
}
