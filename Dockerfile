FROM golang:1.19 AS build

ADD . /src
WORKDIR /src
RUN go build -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo -o remote-follow .

FROM scratch

COPY --from=build /src/remote-follow /remote-follow

ENTRYPOINT ["/remote-follow"]

