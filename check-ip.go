package main

import (
	"errors"
	"net"
)

var (
	nonRoutableNets      []*net.IPNet
	nonRoutableNetRanges = []string{
		"127.0.0.0/8",
		"10.0.0.0/8",
		"172.16.0.0/12",
		"192.168.0.0/16",
		"100.64.0.0/10", // CGNAT
	}

	errNonRoutable      = errors.New("non-routable address")
	errNonGlobalUnicast = errors.New("not a global unicast address")
)

func init() {
	for _, s := range nonRoutableNetRanges {
		_, ipnet, _ := net.ParseCIDR(s)
		nonRoutableNets = append(nonRoutableNets, ipnet)
	}
}

// Verify that the given hostname resolves to a valid (i.e. not
// private, not blocklisted) IP address.
//
// It would be better to perform this same check when we are actually
// establishing the HTTP connection, to avoid potential for
// confusion. Using a local, private DNS resolver cache mitigates
// these concerns however.
//
func checkIP(hostname string) error {
	addrs, err := net.LookupIP(hostname)
	if err != nil {
		return err
	}

	for _, addr := range addrs {
		// Test for loopback, multicast, other-funny-IPv6 addrs.
		if !addr.IsGlobalUnicast() {
			return errNonGlobalUnicast
		}
		// Test against list of non-routable networks.
		for _, ipnet := range nonRoutableNets {
			if ipnet.Contains(addr) {
				return errNonRoutable
			}
		}
	}

	return nil
}
