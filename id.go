package main

import (
	"errors"
	"fmt"
	"net/url"
	"strings"
)

// Identifier representing an ActivityPub actor (user@host).
type identifier struct {
	User string
	Host string
}

func (i *identifier) String() string {
	return fmt.Sprintf("%s@%s", i.User, i.Host)
}

// Build the WebFinger URL for a given identifier.
func (i *identifier) webfingerURL() string {
	args := make(url.Values)
	args.Set("resource", fmt.Sprintf("acct:%s", i.String()))
	return fmt.Sprintf("https://%s/.well-known/webfinger?%s", i.Host, args.Encode())
}

var errBadIdentifier = errors.New("bad identifier")

func parseIdentifier(s string) (*identifier, error) {
	s = strings.TrimPrefix(s, "@")
	parts := strings.Split(s, "@")
	if len(parts) != 2 {
		return nil, errBadIdentifier
	}

	// Just a minor safety check, so that we can't be tricked into
	// making funny WebFinger requests.
	if strings.Contains(parts[1], "/") || strings.Contains(parts[1], ":") {
		return nil, errBadIdentifier
	}

	// Additional safety checks for the remote host.
	if err := checkIP(parts[1]); err != nil {
		return nil, err
	}

	return &identifier{
		User: parts[0],
		Host: parts[1],
	}, nil
}
