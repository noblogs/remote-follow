# remote-follow

*remote-follow* is a "remote follow" application for ActivityPub
accounts. It's based very closely on https://github.com/mwt/apfollow,
but it's easier to deploy (single static binary) and, more
importantly, implements a few checks on the allowed URLs.

As this application needs to perform HTTP requests (for the WebFinger
protocol) based on user-provided data, it tries to do so safely:

* prevent internal network scanning by blocking requests to names that
  resolve to internal or private IP ranges
* rate-limiting? (TODO)
* caching? (TODO)

To simplify implementations, the app also serves a simple SVG "follow
me" button image at the URL */follow.svg*.
